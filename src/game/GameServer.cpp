#include "../../include/game/GameServer.hpp"

void GameServer::runningOrNot()
{
	std::mutex localmutex;
	std::unique_lock<std::mutex> lock(localmutex);
	while (!running && !terminating)
	{
		pausingCondition.wait(lock);
	}
}

GameServer::GameServer(event::EventDispatcher& disp, std::string host)
	: dispatcher{ disp }, hostname{ host }
{
	logger = spdlog::get("mainlogger");
	//Init server
	listener.listen(sf::Socket::AnyPort, sf::IpAddress(hostname));
	port = listener.getLocalPort();
	logger->info("The GameServer is now listening on {}:{}", hostname, port);
}

GameServer::GameServer(event::EventDispatcher& disp, std::string host, short por)
	: dispatcher{ disp }, hostname{ host }, port{ por }
{
	
	logger = spdlog::get("mainlogger");
	listener.listen(port, sf::IpAddress(hostname));
	logger->info("The GameServer is now listening on {}:{}", hostname, port);
}

void GameServer::operator()()
{
	std::mutex localmtx;
	//std::unique_lock<std::mutex> lock(localmtx);
	sf::TcpSocket client;
	while (!terminating)
	{
		runningOrNot();
		//waitingForEvent.wait(lock);
		if (listener.accept(client) == sf::Socket::Done)
		{
			logger->info("Received new connection from {}", client.getRemoteAddress());
		}
	}
}

void GameServer::terminate()
{
	terminating = true;
	logger->debug("GameServer will now terminate!");
	pausingCondition.notify_one();
}

void GameServer::pause()
{
	std::lock_guard<std::mutex> lock(mtx);
	running = false;
	logger->debug("GameServer has now a pause!");
}

void GameServer::cont()
{
	std::lock_guard<std::mutex> lock(mtx);
	running = true;
	logger->debug("GameServer continues his work!");
	pausingCondition.notify_one();
}

bool GameServer::isRunning()
{
	std::lock_guard<std::mutex> lock(mtx);
	return running;
}

bool GameServer::isTerminated()
{
	std::lock_guard<std::mutex> lock(mtx);
	return terminating;
}

short GameServer::getPort()
{
	std::lock_guard<std::mutex> lock(mtx);
	return port;
}


