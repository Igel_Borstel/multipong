#include <iostream>
#include <thread>
#include <functional>
#include <chrono>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::boolalpha;

#include "../include/spdlog/spdlog.h"

#include "../include/event/EventDispatcher.hpp"

#include "../include/game/GameServer.hpp"


int main()
{
	//LOGGER init:
	std::vector<spdlog::sink_ptr> sinks;
	auto logger = spdlog::stdout_logger_mt("mainlogger");
	spdlog::set_pattern("%c [%l] (Thread: %t): %v");
	
	auto log = spdlog::get("mainlogger");

	logger->info("Some Information!");
	logger->warn("A warning!");
	logger->critical("critical message");
	logger->trace("Trace?");
	logger->error("Fehler!");
	logger->debug("Debug");

	event::EventDispatcher dispatcher;
	std::thread dispatcherThread(std::ref(dispatcher));
	dispatcherThread.detach();
	GameServer server(dispatcher, "localhost", 5539);
	std::thread gameServerThread(std::ref(server));
	gameServerThread.detach();
	char c = 'a';
	while (c != 't')
	{
		c = getchar();
	}
	
}

