#ifndef EVENT_BASEEVENT_HPP
#define EVENT_BASEEVENT_HPP

#include <memory>

namespace event
{
	struct BaseEvent
	{
		virtual ~BaseEvent() = default;
	};

	using BaseEventPtr = std::shared_ptr<BaseEvent>;
}

#endif // !EVENT_BASEEVENT_HPP
