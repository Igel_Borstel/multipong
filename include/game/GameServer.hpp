#pragma once

#include <string>

#include <mutex>
#include <condition_variable>
#include "../spdlog/spdlog.h"

#include "../event/EventDispatcher.hpp"
#include <SFML/Network.hpp>

class GameServer
{
	short port;
	std::string hostname;

	bool running{ false };
	bool terminating{ false };

	std::shared_ptr<spdlog::logger> logger;

	std::mutex mtx;
	std::condition_variable pausingCondition;
	std::condition_variable waitingForEvent; //?
	void runningOrNot();

	event::EventDispatcher& dispatcher;
	sf::TcpListener listener;
public:
	GameServer(event::EventDispatcher& disp, std::string host);
	GameServer(event::EventDispatcher& disp, std::string host, short por);
	void operator()();

	virtual void terminate();
	virtual void pause();
	virtual void cont();

	virtual bool isRunning();
	virtual bool isTerminated();

	virtual short getPort();
};